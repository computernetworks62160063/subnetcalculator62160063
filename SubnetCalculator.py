class SubnetCalculator:
    def __init__(self, ip_address, subnet_mask):
        self.ip_address = ip_address
        self.subnet_mask = subnet_mask
        self.ip_binary = self.convert_to_binary(ip_address)
        self.subnet_binary = self.convert_to_binary(subnet_mask)
        self.network_address = self.calculate_network_address()
        self.valid_class = self.check_valid_class()
        

    def convert_to_binary(self, address):
        octets = address.split('.')
        binary_octets = [bin(int(octet)).lstrip('0b').zfill(8) for octet in octets]
        return ''.join(binary_octets)

    def calculate_network_address(self):
        network_address_binary = ''
        for i in range(len(self.ip_binary)):
            network_address_binary += str(int(self.ip_binary[i]) & int(self.subnet_binary[i]))
        network_address = '.'.join(str(int(network_address_binary[i:i+8], 2)) for i in range(0, 32, 8))
        return network_address

    def check_valid_class(self):
        first_octet = int(self.ip_address.split('.')[0])
        if 1 <= first_octet <= 126 and self.ip_binary.startswith('0'):
            return 'Class A \nFrist Octet Range = (1 - 126)'
        elif 128 <= first_octet <= 191 and self.ip_binary.startswith('10'):
            return 'Class B \nFrist Octet Range = (128 - 191)'
        elif 192 <= first_octet <= 223 and self.ip_binary.startswith('110'):
            return 'Class C \nFrist Octet Range = (192 - 223)'
        else:
            return 'Invalid Class'
        
    
    def find_subnet_id(self):
        subnet_id_binary = ''
        for i in range(len(self.ip_binary)):
            subnet_id_binary += str(int(self.ip_binary[i]) & int(self.subnet_binary[i]))
        subnet_id = '.'.join(str(int(subnet_id_binary[i:i+8], 2)) for i in range(0, 32, 8))
        return subnet_id
    
    def convert_to_hex(self, address):
        octets = address.split('.')
        hex_octets = [hex(int(octet)).lstrip('0x') for octet in octets]
        return '.'.join(hex_octets)
        
    def calculate_wildcard_mask(self):
        wildcard_binary = ''.join([''.join('1' if bit == '0' else '0' for bit in octet) for octet in self.subnet_binary.split('.')])
        wildcard_mask = '.'.join(str(int(wildcard_binary[i:i+8], 2)) for i in range(0, 32, 8))
        return wildcard_mask    
    
    def calculate_host_address_range(self):
        network_addr = list(map(int, self.network_address.split('.')))
        wildcard_mask = list(map(int, self.calculate_wildcard_mask().split('.')))
        # Calculate the broadcast address
        broadcast_addr = [255 - wildcard_mask[i] + network_addr[i] for i in range(4)]
        # Calculate the first and last host address in the range
        first_host_addr = '.'.join(str(x) for x in network_addr[:3] + [network_addr[3] + 1])
        last_host_addr = '.'.join(str(x) for x in broadcast_addr[:3] + [broadcast_addr[3] - 1])
        return f"{first_host_addr} - {last_host_addr}"
    
    def calculate_broadcast_address(self):
        wildcard_mask = self.calculate_wildcard_mask()
        wildcard_binary = [bin(int(octet)).lstrip('0b').zfill(8) for octet in wildcard_mask.split('.')]
        broadcast_addr = [str(int(self.network_address.split('.')[i]) | int(wildcard_binary[i], 2)) for i in range(4)]
        return '.'.join(broadcast_addr)
    
    def calculate_subnet_bitmap(self):
        subnet_bitmap = ''
        for i in range(len(self.ip_binary)):
            subnet_bitmap += str(int(self.ip_binary[i]) & int(self.subnet_binary[i]))
        return subnet_bitmap
    
    def calculate_subnet_bits(self):
        subnet_bits = self.subnet_mask.count('0')
        return subnet_bits
    
    def subnet_mask_to_mask_bits(self):
        # แยกแต่ละ octet ใน Subnet Mask
        octets = self.subnet_mask.split('.')
        # เปลี่ยนแต่ละ octet เป็น binary
        binary_octets = [bin(int(octet)).count('1') for octet in octets]
        # นับทั้งหมดเพื่อหา Mask Bits
        mask_bits = sum(binary_octets)
        return mask_bits
    
    def subnet_mask_to_subnet_bits(self):
        # แยกแต่ละ octet ใน Subnet Mask
        octets = self.subnet_mask.split('.')
        # เปลี่ยนแต่ละ octet เป็น binary
        binary_octets = [bin(int(octet)).count('0') for octet in octets]
        # นับทั้งหมดเพื่อหา Subnet Bits
        subnet_bits = sum(binary_octets)
        return subnet_bits 
    
    def calculate_maximum_subnets(self):
        subnet_bits = calculator.subnet_mask_to_subnet_bits()
        maximum_subnets = 2 ** subnet_bits
        return maximum_subnets    
    
    def calculate_hosts_per_subnet(self):
        subnet_bits = calculator.subnet_mask_to_subnet_bits()
        host_bits = 32 - subnet_bits
        hosts_per_subnet = 2 ** host_bits - 2
        return hosts_per_subnet
       
    def display_results(self):
        print(f"----------------------------------------------------------------------")
        print(f"Subnet Calculator")
        print(f"----------------------------------------------------------------------")
        print(f"IP Address: {self.ip_address}")
        print(f"Subnet Mask: {self.subnet_mask}")
        print(f"Network Address: {self.network_address}")
        print(f"Class: {self.valid_class}")
        print(f"Hex IP Address: {calculator.convert_to_hex(ip)}")  # แสดง Hex IP Address
        print(f"Wildcard Mask: {calculator.calculate_wildcard_mask()}")  # แสดง Wildcard Mask
        print(f"Host Address Range: {calculator.calculate_host_address_range()}")
        print(f"Subnet ID: {self.find_subnet_id()}")  # แสดง Subnet ID
        print(f"Broadcast Address: {calculator.calculate_broadcast_address()}")  # แสดง Broadcast Address
        print(f"Subnet Bitmap: {calculator.calculate_subnet_bitmap()}")  # แสดง Subnet Bitmap
        print(f"Subnet Bits: {calculator.subnet_mask_to_subnet_bits()}")
        print(f"Mask Bits: {calculator.subnet_mask_to_mask_bits()}")
        print(f"Maximum Subnets: {calculator.calculate_maximum_subnets()}")
        print(f"Hosts per Subnet: {calculator.calculate_hosts_per_subnet()}")
        print(f"----------------------------------------------------------------------")
      
# Example usage: Tester
if __name__ == "__main__":
    ip = "192.168.0.1"
    subnet = "255.255.255.0"
    calculator = SubnetCalculator(ip, subnet)
    calculator.display_results()




